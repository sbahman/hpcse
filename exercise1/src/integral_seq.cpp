#include <cmath> 
#include <iostream>
#include <iomanip>
#include "timer.hpp"


double integrand(double x){
	return(std::sqrt(x) *std::log(x));

}



int main(int argc, char *argv[]){
	std::cout << "hello world" << std::endl;
	double a = 1, b = 4;
	unsigned long const n = 1e9;
	const double dx = (b-a)/n;

	double sum = 0;
	timer t;

	t.start();
	for(unsigned long i =0; i < n; i++){
		double xi = a + (i+ 0.5)*dx;
		sum += integrand(xi); 
	}
	sum *= dx;
	t.stop();
	std::cout << " Time = " << t.get_timing() << " seconds, Result = " << std::setprecision(8) << sum << std::endl;
	return 0;
}
