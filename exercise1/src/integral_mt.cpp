#include <cmath> 
#include <iostream>
#include <iomanip>
#include <vector>
#include <thread>
#include <mutex>
#include "timer.hpp"


double integrand(double x){
	return(std::sqrt(x) *std::log(x));

}



int main(int argc, char *argv[]){
	double a = 1, b = 4;
	unsigned n = 1e9;
	unsigned int nthreads = 1;
	// Get number of threads from input arguments if available
	if (argc >1)
		nthreads = atoi(argv[1]);
	unsigned long nsteps = n/nthreads;
	if (nsteps *nthreads != n)
	{
		std::cout << "WARNING: n=" << n << " is not a multiple of nthreads=" << nthreads << ". ";
		n = nsteps*nthreads;
		std::cout << "Using n=" << n << " instead." << std::endl;
	}

	const double dx = (b-a)/n;
	
	std::vector<std::thread> threads(nthreads);

	std::pair<double, std::mutex> sum;
	sum.first = 0;
	timer t;

	t.start();
	for (unsigned thr = 0; thr < nthreads; thr++)
	{
#if DBG
		std::cout << "spawning thread " << thr << std::endl;
#endif		
		threads[thr] = std::thread([&sum, a, nsteps, dx, thr]()
		{
			double localSum = 0;
			const double x0 = a + (thr * nsteps + 0.5) * dx; 
			for (unsigned long i = 0; i < nsteps; i++)
			{
				double xi = x0 +i*dx;
				localSum += integrand(xi);
			}
			localSum *= dx;
			std::lock_guard<std::mutex> l(sum.second);
			sum.first += localSum;
		});
	}
	for (std::thread& thr : threads)
		thr.join();

	t.stop();

	std::cout << "Threads = " << nthreads << " Time = " << t.get_timing() << " seconds, Result=" << std::setprecision(8) << sum.first << std::endl;
	return 0;
}
